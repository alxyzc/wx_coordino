<div id="body" class="wrapper" style="background:#fff;
	padding: 15px;
	margin:5px 10px 10px 5px;
	border-radius:10px;
	-webkit-box-shadow:0px 0px 15px 1px rgba(0,0,0,0.32) ,0px 0px 15px 1px rgba(0,0,0,0.12) inset; -moz-box-shadow:0px 0px 15px 1px rgba(0,0,0,0.32) ,0px 0px 15px 1px rgba(0,0,0,0.12) inset; box-shadow:0px 0px 15px 1px rgba(0,0,0,0.32) ,0px 0px 15px 1px rgba(0,0,0,0.12) inset;">
<?
echo $javascript->link('jquery/jquery.js');
echo $javascript->link('jquery/jquery.bgiframe.min.js');
echo $javascript->link('jquery/jquery.ajaxQueue.js');
echo $javascript->link('jquery/thickbox-compressed.js');
?>
<script>
$(document).ready(function(){
	$("#results").show("blind");
	
	function getResults()
	{
	
		$.get("/mini_user_search",{query: $("#UserUsername").val(), type: "results"}, function(data){
		
			$("#results").html(data);
			$("#results").show("blind");
		});
	}	
	
	$("#UserUsername").keyup(function(event){
		getResults();
	});
	
	getResults();

});
</script>
<?=$form->create('User', array('action' => '?'));?>
<?=$form->label('username');?><br/>
 <?=$form->text('username', array('class' => 'big_input', 'autocomplete' => 'off', 'value' => $session->read('errors.data.Post.username')));?><br/>
<span id="title_status"class="quiet">Who are you looking for?</span>
<div id="results" style="overflow: hidden;"></div>
</div>