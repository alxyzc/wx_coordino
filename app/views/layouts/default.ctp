<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?=$title_for_layout;?> | Coordino</title>
	<?=$html->css('screen.css');?>
	<?=$html->css('prettify.css');?>
	<?=$html->script('prettify/prettify.js');?>
	<?=$html->css('skin.css');?>
	<!--[if IE]>
	<style type="text/css">
	  .wrapper {
	    zoom: 1;     /* triggers hasLayout */
	    }  /* Only IE can see inside the conditional comment
	    and read this CSS rule. Don't ever use a normal HTML
	    comment inside the CC or it will close prematurely. */
	</style>
	<![endif]-->	

  <!--[if lte IE 6]><link rel="stylesheet" href="stylesheets/lib/ie.css" type="text/css" media="screen" charset="utf-8"><![endif]-->
</head>
<body onload="prettyPrint()" id="pagebody" style="overflow:auto; background:#efefef;">


<div id="page">

<!--<script>
pagebody= document.getElementById('pagebody');
pagebody.style.width= window.innerWidth+"px";
pagebody.style.height= window.innerHeight+"px";
</script>-->

<div class="wrapperbg" id="header" style="height:100px; position:fixed; z-index:10;">
	<div class="wrapperbgnavlow" style="position:absolute; top:28px; height:72px;"><div class="container" style="height:72px;"><div class="wrapper" style="height:72px;" id="headertabs">

		  <ul class="tabs" style="height:60px;">
		    <li>
		    	<?=$html->link(__('Questions',true),'/');?>
		    </li>
		    <li><?=$html->link(__('Tags',true),'/tags');?></li>
		    <li><?=$html->link(__('Unsolved',true),'/questions/unanswered');?></li>
		    <li><?=$html->link(__('Users',true),'/users');?></li>
			<li class="searchboxli"><? 
				echo $form->create('Post', array('action' => 'display'));
				echo $form->text('needle', array('value' => 'search', 'onclick' => 'this.value=""'));
				echo $form->end();
			?></li>
			<script>
				inputbox= document.getElementById('PostNeedle');
				inputbox.className= "searchbox";
				inputbox.placeholder= "Type Here to Search";
				inputbox.value= ""
			</script>
		  </ul>
		   
		  <ul class="tabs" style="float: right;">
			<li class="ask">
				<?=$html->link(
						__('Ask a question',true),
						array('controller' => 'posts', 'action' => 'ask')
					);
				?>
			</li>
		  </ul>
	</div></div></div>
	
	<div class="wrapperbgnavtop" style="position:absolute; top:0px; height:28px;"><div class="container" style="height:22px;"><div class="wrapper" style="height:22px;">
		<div id="top_actions" class="top_actions">
			
			<ul class="tabs">
				 <? if($session->check('Auth.User.id')) { ?>
					<li class="navlog">
						<?=$html->link(
								$session->read('Auth.User.username'),
								'/users/' . $session->read('Auth.User.public_key') . '/' . $session->read('Auth.User.username')
							);
						?>
					</li>
				<? } ?>
				<? if(!$session->check('Auth.User.id')) { ?>
					<li class="navlog">
					<?=$html->link(
							__('login',true),
							array('controller' => 'users', 'action' => 'login')
						);
					?>
					</li>
				<? } ?>
				<? if(!$session->check('Auth.User.id') || $session->read('Auth.User.registered') == 0) { ?>
				<li class="navreg">
					<?=$html->link(
							__('register',true),
							array('controller' => 'users', 'action' => 'register')
						);
					?>
				</li>
				<? } ?>
				<li>
					<?=$html->link(
							__('about',true),
							array('controller' => 'pages', 'action' => 'display', 'about')
						);
					?>
				</li>
				<? if($session->read('Auth.User.id')) { ?>
				<li>
					<?=$html->link(
							__('settings',true),
							'/users/settings/' . $session->read('Auth.User.public_key')
						);
					?>
				</li>
				<? } ?>
				<li class="chanlang" style="background:none;">
				    <a class="chanlang"><?php __('change language'); ?></a>
				    <div class="tabul">
				        <li><?=$html->link(__('english',true),'/lang/eng')?></li>
				        <li><?=$html->link(__('french',true),'/lang/fre')?></li>
				        <li><?=$html->link(__('chinese',true),'/lang/chi')?></li>
				    </ul>
				</li>
				<? if($session->check('Auth.User.id') && $session->read('Auth.User.permission') != '') { ?>
				<li>
					<?=$html->link(
							__('admin',true),
							array('controller' => 'users', 'action' => 'admin')
						);
					?>
					<ul>
						<li>
							<?=$html->link(
									ucfirst(__('settings',true)),
									array('controller' => 'users', 'action' => 'admin')
								);
							?>
						</li>
						<li>
							<?=$html->link(
									ucfirst(__('Flagged Posts',true)),
									array('controller' => 'users', 'action' => 'flagged')
								);
							?>
						</li>
						<li>
							<?=$html->link(
									ucfirst(__('User Management',true)),
									array('controller' => 'users', 'action' => 'admin_list')
								);
							?>
						</li>
						<li>
							<?=$html->link(
									ucfirst(__('Blacklist',true)),
									array('controller' => 'users', 'action' => 'list_blacklist')
							);
							?>
						</li>
						<li>
							<?=$html->link(
									ucfirst(__('Remote Settings',true)),
									array('controller' => 'users', 'action' => 'remote_settings')
							);
							?>
						</li>
					</ul>
				</li>
				<? } ?>
				
				<? if($session->check('Auth.User.id') && $session->read('Auth.User.registered') == 1) { ?>
				<li>
					<?=$html->link(
							__('logout',true),
							array('controller' => 'users', 'action' => 'logout')
						);
					?>
				</li>
				<? } ?>
			</ul>
		</div>
	</div></div></div>

</div>

<div class="container" style="padding-top:100px; padding-bottom:50px;"><div id="body" class="wrapper">
    <?php echo $session->flash(); ?>
	<div id="content" class="wrapper">
		<?=$content_for_layout;?>
    </div>
    <div id="sidebar" class="wrapper">

		<?
			if(!empty($widgets)) {
				foreach($widgets as $widget) {
		?>
		<div class="widget_box wrapper">
			<? if(!empty($widget['Widget']['title'])) {?>
	      		<h3><?=$widget['Widget']['title'];?></h3>
			<? } ?>
			<?=$widget['Widget']['content'];?>
		<? if(isset($admin) && $admin) { ?>
			<?=$html->link(__('edit', true),'/widgets/edit/' . $widget['Widget']['id'], array('title' => __('Edit this Widget', true)));?>	| 
			<?=$html->link(__('del', true),'/widgets/delete/' . $widget['Widget']['id'], array('title' => __('Delete Widget', true)));?>	
		<? } ?>
		  </div>
		<?
		}
	}
        
	    if(isset($admin) && $admin):
    ?>
	    <?=$html->link($html->image('icons/plugin_edit.png', array('alt' => __('Edit', true))) . __('add widgets to this page', true),
			'/widgets/add' . $html->url(null, false),
			array('escape' => false)
		); ?>
        <? endif; ?>

    </div>
</div></div>


<div class="wrapperbgfooter" id="footer" style="height:24px; position:fixed; bottom:0px;"><div class="container"><div class="wrapper">
	<div style="">
    <ul class="tabs">
      <li>
      <?=$html->link(__('home',true),'/');?></li>
	  <li>
      <?=$html->link(__('ask a question',true),'/questions/ask');?></li>

      <li>
      <?=$html->link(__('about',true),'/about');?></li>
    </ul>

	</div>
	
  </div>


</div></div>

</body>
</html>
